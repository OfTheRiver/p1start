package edu.uprm.cse.datastructures.cardealer.util;

//Reverse Iterator interface for my reverse iterators!
public interface ReverseIterator<E>{
	
	public boolean hasPrevious();

	public E previous();

}
