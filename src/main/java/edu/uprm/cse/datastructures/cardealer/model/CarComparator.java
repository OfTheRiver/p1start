package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	
	//Custom comparator that compares car1 with car2
	@Override
	public int compare(Car car1, Car car2) {
		// First compares the brand of the two Cars
		if (car1.getCarBrand().compareTo(car2.getCarBrand()) == 0) {
			// Then it compares the Model of each car
			if (car1.getCarModel().compareTo(car2.getCarModel()) == 0) {
				//Finally it compares the Model Option of each car
				if (car1.getCarModelOption().compareTo(car2.getCarModelOption()) != 0) {
					return car1.getCarModelOption().compareTo(car2.getCarModelOption());
				} else {
					return 0;
				}
			} else {
				return car1.getCarModel().compareTo(car2.getCarModel());
			}
		} else {
			return car1.getCarBrand().compareTo(car2.getCarBrand());
		}

	}


}