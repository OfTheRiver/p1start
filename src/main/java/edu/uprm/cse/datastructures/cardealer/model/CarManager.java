package edu.uprm.cse.datastructures.cardealer.model;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {

	private static CircularSortedDoublyLinkedList<Car> carList = CarList.getInstance();

	//Get method that allows us to retrieve all the cars we currently have on the carList instance
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] temp = new Car[carList.size()];
		int index = 0;
		while (index < temp.length) {
			temp[index] = carList.get(index);
			index++;
		}
		return temp;
	}

	//Get method that allows us to retrieve a specific car from the carList by searching its ID
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		Car temp = new Car();
		for (int i = 0; i < carList.size(); i++) {
			if (carList.get(i).getCarId() == id) {
				temp = carList.get(i);
				return temp;
			}
		} 
		throw new WebApplicationException(404);
	}

	//Add method which allows us to add a Car to the carList
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
//		for (int i = 0; i < carList.size(); i++) {
//			if (carList.get(i).getCarId() == car.getCarId()){
//				return Response.status(Response.Status.BAD_REQUEST).build();
//			}
//		}
		
		carList.add(car);
		return Response.status(201).build();
	}

	//Remove method which allows us to delete a car entry from the carList utilizing its ID
	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {
		for (int i = 0; i < carList.size(); i++) {
			if (carList.get(i).getCarId() == id) {
				carList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		} 
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	//Update method that allows us to change any and all of the desired car's values in the carList
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		for (int i = 0; i < carList.size(); i++) {
			if (carList.get(i).getCarId() == car.getCarId()) {
				carList.remove(i);
				carList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

}
