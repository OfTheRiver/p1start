package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;




public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public Node() {
			this.element = null;
			this.next = this.prev = null;

		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}

	private Node<E> header;
	private int currentSize;
	private Comparator<E> comparator;

	public CircularSortedDoublyLinkedList(Comparator<E> cmp) {
		this.currentSize = 0;
		this.header = new Node<>();

		this.header.setNext(this.header);
		this.header.setPrev(this.header);

		this.comparator = cmp;
	}

	//Allows us to iterate fowards
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;

		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}
		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		} 

		@Override
		public E next() {
			if (this.hasNext()) {

				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}

	//Allows us to iterate fowards from a specific index
	private class CircularSortedDoublyLinkedListIndexIterator<E> implements Iterator<E>{
		private Node<E> nextNode;

		public CircularSortedDoublyLinkedListIndexIterator(int index) {
			this.nextNode = (Node<E>) header.getNext();
			int i = 0;
			while(i<index) {
				this.nextNode = this.nextNode.getNext();
				i++;
			}
		}
		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		} 

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}

	//Inherits Reverse iterator interface, allowing us to iterate backwards
	private class LinkedListReverseIterator<E> implements ReverseIterator<E>{
		private Node<E> prevNode;

		public LinkedListReverseIterator() {
			this.prevNode = (Node<E>) header.getPrev();
		}

		public boolean hasPrevious() {
			return prevNode.getElement() != null;
		}

		public E previous() {
			if(this.hasPrevious()) {
				E result = this.prevNode.getElement();
				this.prevNode = this.prevNode.getPrev();
				return result;
			} else throw new NoSuchElementException();
		}

	}

	//Inherits Reverse iterator interface, allowing us to iterate backwards from a specified index
	private class LinkedListReverseIndexIterator<E> implements ReverseIterator<E>{
		private Node<E> prevNode;

		public LinkedListReverseIndexIterator(int i) {
			this.prevNode = (Node<E>) header.getPrev();
			int index = currentSize-1;
			while(index != i) {	prevNode = prevNode.getPrev(); index--;	}
		}


		public boolean hasPrevious() {
			return prevNode.getElement() != null;
		}

		public E previous() {
			if(this.hasPrevious()) {
				E result = this.prevNode.getElement();
				this.prevNode = this.prevNode.getPrev();
				return result;
			} else throw new NoSuchElementException();
		}
	}



	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	public Iterator<E> iterator(int index){
		return new CircularSortedDoublyLinkedListIndexIterator<E>(index);
	}

	public ReverseIterator<E> reverseIterator() { 
		return new LinkedListReverseIterator<E>();			
	}

	public ReverseIterator<E> reverseIterator(int index) { 
		return new LinkedListReverseIndexIterator<E>(index);
	}

	//Method that allows us to add an object to the list and mantain the list sorted throught the use of comparator
	@Override
	public boolean add(E obj) {
		//If the object is null then it WONT be added
		if(obj == null) {
			return false;
		}

		//This node will be the one we use to set our object
		Node<E> newNode = new Node<E>();
		newNode.setElement(obj);

		//If it is empty, the newNode will be added after and before the header (as it is a circular linked list)
		if(this.isEmpty()) {
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			newNode.setNext(this.header);
			newNode.setPrev(this.header);
			this.currentSize++;
			return true;
		}

		//Temp shall be used to iterate and sort the list
		Node<E> temp = this.header.getNext();
		newNode.setElement(obj);

		//This method will run as long as the list isnt empty
		while (this.size() > 0) {
			//if the parameter object and the object in temp are the same then the element in temp will be added before the object already in the list
			if(this.comparator.compare(temp.getElement(), newNode.getElement()) == 0) {
				newNode.setPrev(temp.getPrev());
				temp.getPrev().setNext(newNode);
				temp.setPrev(newNode);
				newNode.setNext(temp);
				this.currentSize++;
				return true;
			} //Else if temp is the last node and its element is "smaller" than that of the parameter object, 
			  //then the parameter object will be added at the end of the list and after the temp
			else if(comparator.compare(temp.getElement(), newNode.getElement()) < 0 && temp.getNext().getElement() == null) {
				newNode.setNext(header);
				this.header.setPrev(newNode);
				temp.setNext(newNode);
				newNode.setPrev(temp);
				this.currentSize++;
				return true;
			}  //Else if temp's previous reference's element is null (header) and, when comparing temp's element with the parameter element, 
			   //we receive that temp's element is larger than the parameter we place the temp after the parameter's node
			   //and place the parameter after the header (first in the list)
			else if(temp.getPrev().getElement() == null && comparator.compare(temp.getElement(), newNode.getElement()) > 0) {
				newNode.setNext(temp);
				temp.setPrev(newNode);
				header.setNext(newNode);
				newNode.setPrev(header);
				this.currentSize++;
				return true;
			} 	//If temp is smaller than the parameter element but temp's next reference is bigger than the parameter
				//then the parameter's node will be placed in between temp and temp's next reffernce
				else if(comparator.compare(temp.getElement(), newNode.getElement()) < 0 && comparator.compare(temp.getNext().getElement(), newNode.getElement()) > 0) {
				newNode.setNext(temp.getNext());
				temp.getNext().setPrev(newNode);
				temp.setNext(newNode);
				newNode.setPrev(temp);
				this.currentSize++;
				return true;
			} 	//Finally, if temp's next element is null (header) then we place the parameter element's node at the end
				//because it means we've iterated through the whole list!
				else if(temp.getNext().getElement() == null) {
				temp.setNext(newNode);
				newNode.setPrev(temp);
				newNode.setNext(this.header);
				this.header.setPrev(newNode);
				this.currentSize++;
				return true;
			}
			temp = temp.getNext();

		}
		return false;

	}


	// Method returns List's current size
	@Override
	public int size() {
		return this.currentSize;
	}

	//Method to remove a specific element
	@Override
	public boolean remove(E elmnt) {
		//If list is empty it cannot remove anything, therefore it returns false
		if (elmnt == null) {
			return false;
		}

		//These two variables will be used to iterate through the list
		Node<E> newNode = this.header.getNext();
		int count = 0;

		//Uses a count to iterate through the list
		while (count < this.size()) {
			//If the parameter element is the same as the element in newNode, we turn the newNode's next and
			//prev references to null and link its prev and next references with each other
			if (this.comparator.compare(newNode.getElement(), elmnt) == 0) {
				newNode.getPrev().setNext(newNode.getNext());
				newNode.getNext().setPrev(newNode.getPrev());
				newNode.setElement(null);
				newNode.setNext(null);
				newNode.setPrev(null);
				this.currentSize--;
				return true;
			} else if (this.comparator.compare(newNode.getElement(), elmnt) != 0) {
				newNode = newNode.getNext();
				count++;
			}
		}
		return false;
	}

	//Remove method which removes the element at a certain index
	@Override
	public boolean remove(int index) {
		if ((index < 0) || (index >= this.currentSize)){
			throw new IndexOutOfBoundsException();
		}
		else {
			//Objects we will use to iterate
			Node<E> newNode = this.header.getNext();
			int currentPos =0;

			//While the currentPos value isnt the same as the index's value, we shall keep iterating through the list
			while (currentPos != index) {
				newNode = newNode.getNext();
				currentPos++;
			}

			//As soon as the index and the current position have the same value we change the newNode's next and prev
			//references to reference each other and newNode's next and prev will turn null
			newNode.getPrev().setNext(newNode.getNext());
			newNode.getNext().setPrev(newNode.getPrev());
			newNode.setElement(null);
			newNode.setNext(null);
			newNode.setPrev(null);
			this.currentSize--;
			return true;
		}
	}

	//Method to remove all cases of element "obj" in the list
	@Override
	public int removeAll(E obj) {
		int count = 0; //Will be used to iterate
		//While it is possible for us to remove the object in the list, we will continue iterating until this case becomes false
		while (this.remove(obj)) {
			count++;
		}
		//Returns total amount of that object that was removed from the list
		return count;
	}

	//Returns first object in list
	@Override
	public E first() {
		return this.get(0);
	}

	//Returns last object in list
	@Override
	public E last() {
		return this.get(this.size()-1);
	}

	//Gets object in the specified index
	@Override
	public E get(int index) {
		//If specified index is too big or too small, the method will throw an error
		if(index < 0 || this.size() <= index) {
			throw new IndexOutOfBoundsException("This index isnt in the list");
		}

		//Values used to iterate throughout the loop
		int currentPos = 0;
		Node<E> temp = this.header.getNext(); 

		//While the currentPos value isnt the same as the index's value, we shall keep iterating through the list
		while (currentPos != index) {
			temp = temp.getNext();
			currentPos++;
		}

		//As soon as the index and the current position have the same value it will return the element inside the temp node
		return temp.getElement();
	}

	//Method to remove every object inside list
	@Override
	public void clear() {
		//While the list isnt empty, removes the first object in the list
		while (!this.isEmpty()) {
			this.remove(0);
		}
	}

	//Checks to see if the object is in the list
	@Override
	public boolean contains(E e) {
		//If the object is in the list it must have an index, therefore if it is bigger than 0 it must be in the list
		return this.firstIndex(e) >= 0;
	}

	//Checks to see if the list's size is 0
	@Override
	public boolean isEmpty() {
		if (this.size() == 0) {
			return true;
		}
		return false;
	}

	//Method to verify the index where an object is found the first time in a list
	@Override
	public int firstIndex(E e) {
		//Values for iterating
		Node<E> temp = this.header.getNext();
		int currentPos = 0;
		//While current Position is less than size, iterate through the list
		while (currentPos < this.size()) {
			//If both the parameter element and the element in temp are the same then it will return the position we were in
			//if not, it will keep iterating
			if(this.comparator.compare(temp.getElement(), e) == 0) { 
				return currentPos;
			} else if(this.comparator.compare(temp.getElement(), e) != 0) {
				temp = temp.getNext();
				currentPos++;
			}
		}
		//If it is never found it will return -1
		return -1;
	}

	//Method to verify the index where an object is found the last time in a list
	@Override
	public int lastIndex(E e) {
		//Values for iterating
		Node<E> temp = this.header.getPrev();
		int count = this.currentSize - 1;
		//While the count is bigger than 0 it will continue iterating from the back of the list
		while(count > 0) {
			//If the parameter element is the same as the current element in temp then it will return the index it is found in   
			if(this.comparator.compare(temp.getElement(), e) == 0) { 
				return count;
			} else if(this.comparator.compare(temp.getElement(), e) != 0) { //If the parameter element and the temp's element arent the same it keeps iterating
				count--;
				temp = temp.getPrev();
			}
		}
		//If the element is never found it will return -1
		return -1;
	}


}
