package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

//Class made to create and instance of a singleton carList to be utilized in the REST API
public class CarList {
	private static CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return carList;
	}
	
	//Allows us to reset our carList, removing any and all values previously added to it
	public static void resetCars() {
		carList.clear();
	}
}
